var mongoose = require('mongoose');
var Grid = require('gridfs-stream');
var File = mongoose.model('File');

module.exports.upload = function(req, res) {
    var gfs = new Grid(mongoose.connection.db, mongoose.mongo);

    var userId = mongoose.Types.ObjectId(req.payload._id);
    var part = req.files.file;

    var writeStream = gfs.createWriteStream({
        filename: part.name,
        content_type: part.mimetype,
        mode: 'w'
    });

    writeStream.on('close', function(savedFile) {
        var file = new File({
            fileInfo: savedFile,
            uploadedBy: userId,
            downloadUrl: '/api/files/' + savedFile._id.toString()
        });

        file.save(function(err){
            if(err){
                res.status(500).json("Item not saved");
            } else {
                res.status(200).json({ file: file });
            }
        });

    });

    writeStream.write(part.data);
    writeStream.end();

}

module.exports.download = function(req, res) {
    var gfs = new Grid(mongoose.connection.db, mongoose.mongo);
    var fileId = mongoose.Types.ObjectId(req.params.id); 
    gfs.files.findOne({ _id: fileId }, function(err, file){
        if(err){
            res.status(404).json("Not found");
        } else {
            res.writeHead(200, { "Content-Type": file.contentType });
            var readStream = gfs.createReadStream({
                _id: fileId
            });
            readStream.pipe(res);
        }
    });
}


module.exports.getAllByUser = function(req, res) {
    var userId = mongoose.Types.ObjectId(req.payload._id);
    File.find({ uploadedBy: userId })
        .populate('fileInfo')
        .exec(function(err, files){
            if(err){
                res.status(404).json("Not found");
            } else {
                res.status(200).json({ files: files });
            }
        });    
}