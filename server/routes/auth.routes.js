var express = require('express');
var router = express.Router();

var AuthController = require('../controllers/auth.controller');

router.route('/register')
    .post(AuthController.register);

router.route('/login')
.post(AuthController.login);

module.exports = router;