var express = require('express');
var router = express.Router();

var authRoutes = require('./auth.routes');
var filesRoutes = require('./files.routes');


router.use('/auth', authRoutes);
router.use('/files', filesRoutes);

module.exports = router;