var config = {
    port: 3000,
    host: 'localhost',
    root: '../client/dist'
}
module.exports = config;