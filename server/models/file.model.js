var mongoose = require('mongoose')
var GFS = mongoose.model('GFS', new mongoose.Schema({}, { strict: false }), "fs.files");
var User = mongoose.model('User');

var FileSchema = new mongoose.Schema({
    fileInfo: { type: mongoose.Schema.Types.ObjectId, ref: "GFS" },
    uploadedBy: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
    downloadUrl: String
});

var File = mongoose.model("File", FileSchema);

module.exports = File;