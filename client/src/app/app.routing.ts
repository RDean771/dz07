import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FilesComponent } from './components/files/files.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { isAuthenticatedGuard } from './services/is-authenticated-guard.service';
import {isNotAuthenticatedGuard} from './services/is-not-authenticated-guard.service';
import { AccountComponent } from './components/account/account.component';



const appRoutes: Routes = [
    //{ path: '', redirectTo: 'files', pathMatch: 'full'},
    { path: 'files', canActivate:[isAuthenticatedGuard], component: FilesComponent },
    { path: 'login', canActivate:[isNotAuthenticatedGuard],component: LoginComponent },
    { path: 'register', canActivate:[isNotAuthenticatedGuard],component: RegisterComponent },
    { path: 'account', component:AccountComponent, canActivate:[isAuthenticatedGuard] },
    { path: '**', redirectTo: 'files' }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}