import { FileMetadata } from "../../classes/file-metadata";

let data: FileMetadata[] = [];

let item1: FileMetadata = new FileMetadata("doc.docx", "10.10.2010.", 5);
let item2: FileMetadata = new FileMetadata("item.pdf", "10.10.2015.", 5.14);
let item3: FileMetadata = new FileMetadata("table.xls", "10.10.2010.", 2.74);
data.push(item1);
data.push(item2);
data.push(item3);

export default data;
