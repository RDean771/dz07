import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validator, Validators, AbstractControl, FormBuilder} from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Token } from '../../classes/token';
import { User } from '../../classes/user';
import { RegisterCredentials} from '../../classes/register-credentials';
import { AuthService} from '../../services/auth.service';
import { ValidateEmailNotTaken} from '../../services/async-email-validator';
@Component({
  selector: 'app-edit-info-modal',
  templateUrl: './edit-info-modal.component.html',
  styleUrls: ['./edit-info-modal.component.css']
})
export class EditInfoModalComponent implements OnInit {

  private userForm: FormGroup;
  
  public payload;
  public token;
  constructor(public activeModal: NgbActiveModal, public authService:AuthService, private fb:FormBuilder) {
    this.token = authService.getToken();
    this.payload = Token.parseToken(this.token);
    console.log(this.payload);
   }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.userForm = this.fb.group({
      firstName: [''],
      lastName: [''],
      email: ['',[Validators.email],ValidateEmailNotTaken.createValidator(this.authService)],
      password: ['123']
    });
  
  }
  public showError: boolean = false;

  onSubmit() {
    let formValue: any = this.userForm.value;
    delete formValue['confirmPassword'];
    let credentials: RegisterCredentials = formValue;
    this.activeModal.close(false)
    this.authService.register(credentials, (user: User)=>{
      console.log(user);
    }, (err: any)=>{
      console.log(err);
    });
    this.authService.logout();
    this.authService.login(credentials, (user: User)=>{
      console.log(user);
    }, (err: any)=>{
      console.log(err);
    });
    
  }
}
