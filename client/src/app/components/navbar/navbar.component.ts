import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

import { User } from '../../classes/user';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  user: User = null;
  loggedIn: boolean = false;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.subscribeForUser();
  }

  subscribeForUser() {
    this.authService
      .user.subscribe((user: User) => {
        this.user = user;
      });
    this.authService
      .loggedIn.subscribe((isLoggedIn: boolean) => {
        this.loggedIn = isLoggedIn;
      });
  }

}
