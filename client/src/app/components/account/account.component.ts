import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { User } from '../../classes/user';
import { Token } from '../../classes/token';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditInfoModalComponent} from '../edit-info-modal/edit-info-modal.component';
@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  public payload;
  public token;
  constructor(public authService:AuthService, public modalService:NgbModal) {
    this.token = authService.getToken();
    this.payload = Token.parseToken(this.token);
   }
  ngOnInit() {
  }
  openEditInfoModal() {
    const modalRef = this.modalService.open(EditInfoModalComponent, {backdrop: true, size: 'lg'});
    modalRef.result.then((result: any) => {
      console.log(result);
    }, (dismiss: any)=> {
      console.log(dismiss);
    });

  }

}
