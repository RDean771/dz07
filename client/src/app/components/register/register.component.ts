import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validator, Validators, AbstractControl } from '@angular/forms';

import { LoginCredentials } from '../../classes/login-credentials';
import { RegisterCredentials } from '../../classes/register-credentials';
import { User } from '../../classes/user';
import { AuthService } from '../../services/auth.service';
import { ThrowStmt } from '@angular/compiler';

function passwordConfirming(c: AbstractControl): any {
  if(!c.parent || !c) return;
  const pwd = c.parent.get('password');
  const cpwd= c.parent.get('confirmPassword')

  if(!pwd || !cpwd) return;
  if (pwd.value !== cpwd.value) {
      return { invalid: true };
  }
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public userForm: FormGroup;

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.userForm = new FormGroup({
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
      confirmPassword: new FormControl('', [Validators.required, passwordConfirming])
    });
  }

  public showError: boolean = false;

  onSubmit() {
    let formValue: any = this.userForm.value;
    delete formValue['confirmPassword'];
    let credentials: RegisterCredentials = formValue;
    this.authService.register(credentials, (user: User)=>{
      console.log(user);
    }, (err: any)=>{
      console.log(err);
    });
    
  }

}
