class FileInfo {
    constructor(
        public _id: string,
        public aliases: any,
        public chunkSize: number,
        public conentType: string,
        public fileName: string,
        public length: string,
        public md5: string,
        public metadata: any,
        public uploadDate: string
    ){}
}

export class FileItem {
    constructor(
        public downloadUrl: string,
        public uploadedBy: string,
        public _id: string,
        public fileInfo: FileInfo
    ) { }

    static setImage(item: FileItem) {
        let split: string[] = item.fileInfo.fileName.split(".");
        let path: string = "../../../assets/images/";
    
        let imageName: string = "";
        
        if(split.length == 1) {
          imageName = "file.png";
        } else {
          let lastStr = split[split.length - 1];
          switch(lastStr) {
            case "jpg":
              imageName = "jpg.png";
              break;
            case "mp3":
              imageName = "mp3.png";
              break;          
            case "pdf":
              imageName = "pdf.png";
              break;          
            case "ppt":
              imageName = "ppt.png";
              break;
            case "pptx":
              imageName = "ppt.png";
              break;              
            case "txt":
              imageName = "txt.png";
              break;   
            case "doc":
              imageName = "word.png";
              break;
            case "docx":
              imageName = "word.png";
              break;                          
            case "xls":
              imageName = "xls.png";
              break;
            case "xlsx":
              imageName = "xls.png";
              break;                        
            default:
              imageName = "file.png"
          }
        }
    
        return path + imageName;
      }    

}
