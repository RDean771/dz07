import { AbstractControl } from '@angular/forms';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
@Injectable()
export class ValidateEmailNotTaken {
  static createValidator(signupService: AuthService) {
    return (control: AbstractControl) => {
      return signupService.checkEmailNotTaken(control.value).map(res => {
        return res ? null : { emailTaken: true };
      });
    };
  }
}