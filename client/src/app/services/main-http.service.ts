import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MainHttpService {

  constructor(private http: HttpClient, private router: Router) { }

  createAuthRequestOptions(): { headers: HttpHeaders } {
    let headers = new HttpHeaders({
     'Content-Type': 'application/json',
     'x-auth-token': "" 
    });

    return { headers } ;
  }

  get<T>(url: string): Observable<any> {
    return this.http.get(url, this.createAuthRequestOptions());
  }

  post<T>(url: string, item: T): Observable<any> {
    return this.http.post(url, JSON.stringify(item), this.createAuthRequestOptions());
  }

  put<T>(url: string, item: T): Observable<any> {
    return this.http.put(url, JSON.stringify(item), this.createAuthRequestOptions());
  }

  delete<T>(url: string): Observable<any> {
    return this.http.delete(url, this.createAuthRequestOptions());
  }

}
