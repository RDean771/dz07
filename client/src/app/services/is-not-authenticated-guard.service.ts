import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
@Injectable()
export class isNotAuthenticatedGuard implements CanActivate {
  constructor(public auth: AuthService, public router: Router) {}
  canActivate(route,state): boolean {
    return !this.auth.isLoggedIn();
  }
}