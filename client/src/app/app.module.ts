import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule, EmailValidator } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Http, HttpModule} from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FilesComponent } from './components/files/files.component';
import { SectionHeaderComponent } from './components/section-header/section-header.component';
import { TableComponent } from './components/files/table/table.component';
import { LoginComponent } from './components/login/login.component';
import { AppRoutingModule } from './app.routing';
import { RegisterComponent } from './components/register/register.component';
import { FileUploadModalComponent } from './components/file-upload-modal/file-upload-modal.component';

import { RemoveHostDirective } from './directives/remove-host.directive';

import { MainHttpService } from './services/main-http.service';
import { AuthService } from './services/auth.service';
import { isAuthenticatedGuard } from './services/is-authenticated-guard.service';
import { isNotAuthenticatedGuard } from './services/is-not-authenticated-guard.service';
import { AccountComponent } from './components/account/account.component';
import { EditInfoModalComponent } from './components/edit-info-modal/edit-info-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FilesComponent,
    SectionHeaderComponent,
    TableComponent,
    RemoveHostDirective,
    LoginComponent,
    RegisterComponent,
    FileUploadModalComponent,
    AccountComponent,
    EditInfoModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    HttpModule
  ],
  providers: [
    MainHttpService,
    AuthService,
    isAuthenticatedGuard,
    isNotAuthenticatedGuard,
    EmailValidator
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    FileUploadModalComponent,
    EditInfoModalComponent
  ]
})
export class AppModule { }
